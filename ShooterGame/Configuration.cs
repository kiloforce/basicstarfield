﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShooterGame
{
    class Configuration
    {
        public const int WINDOW_WIDTH = 600;
        public const int WINDOW_HEIGHT = 600;
        public const int ASTEROID_DESTROY_SCORE = 5;
        public const int ENEMY_SHIP_DESTROY_SCORE = 20;
        public const int ASTEROID_COLLIDE_DAMAGE = 20;
        public const int ENEMY_SHIP_COLLIDE_DAMAGE = 40;
        public const int ENEMY_BULLET_COLLIDE_DAMAGE = 10;
    }

    public static class PlayerConstants
    {
        public const int MAX_HEALTH = 200;
        public const int NORMAL_SPEED = 10;
        public const int NORMAL_BULLET_DELAY = 20;
    }

    public static class EnemyConstants
    {

    }

    public static class PowerupConstants
    {
        public const int HEALTH_INCREASE = 20;
        public const int RAPID_FIRE_DELAY_DECREASE = 10;
        public const int RAPID_FIRE_TIME = 500;
    }
}
