﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace ShooterGame
{
    public class Asteroid
    {
        public Rectangle boundingBox;
        public Texture2D texture;
        public Vector2 position, origin;
        public float rotationAngle;
        public int speed;

        public bool isVisible;
        Random random = new Random();
        public float randX, randY;

        //Constructor
        public Asteroid(Texture2D newTexture, Vector2 newPosition)
        {
            texture = newTexture;
            position = newPosition;
            speed = 4;
            isVisible = true;
            randX = random.Next(0, 500);
            randY = random.Next(-600, -50);
        }

        //Load Content
        public void LoadContent(ContentManager Content)
        {
            
        }

        //Update
        public void Update(GameTime gameTime)
        {
            //Set bounding for collision
            boundingBox = new Rectangle((int)position.X, (int)position.Y, texture.Width, texture.Height);

            origin.X = texture.Width / 2;
            origin.Y = texture.Height / 2;

            //Update movement
            position.Y = position.Y + speed;

            if (position.Y >= 600)
            {
                position.Y = -50;
            }

            //Rotate Asteroid
            float elapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;
            rotationAngle += elapsed;
            float circle = MathHelper.Pi * 2;
            rotationAngle = rotationAngle % circle;
        }

        //Draw
        public void Draw(SpriteBatch spriteBatch)
        {
            if (isVisible)
            {
                spriteBatch.Draw(texture, position, null, Color.White, rotationAngle, origin, 1.0f, SpriteEffects.None, 0f);
            }
        }//End Draw function
    }
}
