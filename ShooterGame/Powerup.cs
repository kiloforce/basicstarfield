﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace ShooterGame
{
    public class Powerup
    {
        public enum PowerupType
        {
            Health = 0,
            MachineGun
        }
        public Texture2D texture;
        public Vector2 position;
        public Rectangle boundingBox;
        public PowerupType powerupType;
        public int speed;
        public bool isVisible;

        //Constructor
        public Powerup(Texture2D newTexture, Vector2 newPosition, PowerupType newPowerupType = PowerupType.Health, int newSpeed = 2)
        {
            texture = newTexture;
            position = newPosition;
            powerupType = newPowerupType;
            speed = newSpeed;
            isVisible = true;
        }//End Constructor

        //LoadContent
        public void LoadContent(ContentManager contentManager)
        {
        }//End LoadContent method

        //Update
        public void Update(GameTime gameTime)
        {
            boundingBox = new Rectangle((int)position.X, (int)position.Y, texture.Width, texture.Height);
            position.Y = position.Y + speed;

            if (position.Y >= Configuration.WINDOW_HEIGHT)
                isVisible = false;

        }//End Update method

        //Draw
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, position, Color.White);
        }//End Draw method

    }
}
