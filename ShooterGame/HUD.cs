﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace ShooterGame
{
    public class HUD
    {
        public int playerScore, screenWidth, screenHeight, healthBarHeight, health;
        public SpriteFont playerScoreFont;
        public Vector2 playerScorePos, healthBarPosition;
        public bool showHUD;
        public Texture2D healthTexture;
        public Rectangle healthRectangle;

        //Constructor
        public HUD()
        {
            playerScore = 0;
            showHUD = true;
            screenHeight = 600;
            screenWidth = 600;
            playerScoreFont = null;
            playerScorePos = new Vector2(screenWidth/2, 5);
            healthBarPosition = new Vector2(15, 15);
            healthBarHeight = 15;
            healthTexture = null;
        }

        //Load Context
        public void LoadContent(ContentManager Content)
        {
            playerScoreFont = Content.Load<SpriteFont>("georgia");
            healthTexture = Content.Load<Texture2D>("healthbar");
        }

        //Update
        public void Update(GameTime gameTime)
        {
            // Get keyboard state
            KeyboardState KBS = Keyboard.GetState();

            //Set rectangle for healthbar
            healthRectangle = new Rectangle((int)healthBarPosition.X, (int)healthBarPosition.Y, Player.player.health, healthBarHeight);
        }

        //Draw
        public void Draw(SpriteBatch spriteBatch)
        {
            //if we are showing our HUD
            if (showHUD)
            {
                spriteBatch.DrawString(playerScoreFont, "Score - " + playerScore, playerScorePos, Color.Red);
                spriteBatch.Draw(healthTexture, healthRectangle, Color.White);
            }
        }
    }
}
