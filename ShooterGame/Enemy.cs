﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace ShooterGame
{
    public class Enemy
    {
        public Rectangle boundingBox;
        public Texture2D texture, bulletTexture;
        public Vector2 position;
        public int health, speed, bulletDelay, currentDifficultyLevel;
        public bool isVisible;
        public List<Bullet> bulletList;
        Random random = new Random();

        //Constructor
        public Enemy(Texture2D newTexture, Vector2 newPosition, Texture2D newBulletTexture)
        {
            texture = newTexture;
            bulletTexture = newBulletTexture;
            position = newPosition;
            bulletList = new List<Bullet>();
            health = 5;
            currentDifficultyLevel = 1;
            bulletDelay = 100;
            speed = 5;
            isVisible = true;
        }

        //Update
        public void Update(GameTime gameTime)
        {
            //update collision rectangle
            boundingBox = new Rectangle((int)position.X, (int)position.Y, texture.Width, texture.Height);

            //update enemy movement
            position.Y = position.Y + speed;

            //move enemy back to top of screen if he flys off bottom
            if (position.Y >= 600)
            {
                position.Y = -50;
                position.X = random.Next(0, 550);
            }

            EnemyShoot();
            UpdateBullets();
        }

        //Draw
        public void Draw(SpriteBatch spriteBatch)
        {
            //Draw enemy ship
            spriteBatch.Draw(texture, position, Color.White);

            //Draw enemy bullets
            foreach (Bullet b in bulletList)
            {
                b.Draw(spriteBatch);
            }
        }

        public void UpdateBullets()
        {
            //for each bullet in our bulletlist, update the movement and if the bullet hits the top of the screen remove it from list
            foreach (Bullet b in bulletList)
            {
                //Boundingbox for bullets
                b.boundingBox = new Rectangle((int)b.position.X, (int)b.position.Y, 10, 10);

                //set movement for bullet
                b.position.Y = b.position.Y + b.speed;

                //if bullet hits the bottom of the screen, then make visible false
                if (b.position.Y >= 600)
                {
                    b.isVisible = false;
                }
            }

            //Iterate through bulletlist and see if any of the bullets are not visible, if they arent then remove from bulletlist
            for (int i = 0; i < bulletList.Count; i++)
            {
                if (!bulletList[i].isVisible)
                {
                    bulletList.RemoveAt(i);
                    i--;
                }
            }
        }

        //Enemy shoot function
        public void EnemyShoot()
        {
            //Shoot only if bulletdelay resets
            if (bulletDelay > 0)
            {
                bulletDelay--;
            }

            if (bulletDelay <= 0)
            {
                //Create new bullet and place in fornt and center of enemy ship
                Bullet newBullet = new Bullet(bulletTexture);
                newBullet.position = new Vector2(position.X+texture.Width/2-newBullet.texture.Width/2, position.Y + 30);
                newBullet.isVisible = true;
                newBullet.speed = 8;
                if (bulletList.Count < 20)
                {
                    bulletList.Add(newBullet);
                }
                bulletDelay = 100;
            }
        }//End EnemyShoot function
    }
}
