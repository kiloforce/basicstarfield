﻿#region Using Statements
using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
#endregion

namespace ShooterGame
{
    public class Game1 : Game
    {
        //State enum
        public enum State
        {
            Menu,
            Playing,
            Gameover
        }

        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Random random = new Random();
        public int enemyBulletDamage;
        public Texture2D menuImage;
        public Texture2D gameoverImage;
        public float musicLevel = 0.9f;
        

        //Lists
        List<Asteroid> asteroidList = new List<Asteroid>();
        List<Enemy> enemyList = new List<Enemy>();
        List<Explosion> explosionList = new List<Explosion>();
        List<Powerup> powerupList = new List<Powerup>();

        Player player = new Player();
        StarField starField = new StarField();
        HUD hud = new HUD();
        SoundManager soundManager = new SoundManager();

        //Set first state
        State gameState = State.Menu;

        public Game1(): base()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.IsFullScreen = false;
            graphics.PreferredBackBufferWidth = 600;
            graphics.PreferredBackBufferHeight = 600;
            this.Window.Title = "Monogame 2D Space Shooter";
            Content.RootDirectory = "Content";
            enemyBulletDamage = 10;
            menuImage = null;
            gameoverImage = null;
        }

        //Init
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }//End Initialize method

        //Load Content
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            player.LoadContent(Content);
            starField.LoadContent(Content);
            hud.LoadContent(Content);
            soundManager.LoadContent(Content);
            menuImage = Content.Load<Texture2D>("MainMenuImage");
            gameoverImage = Content.Load<Texture2D>("GameoverImage");

        }//End LoadContent method

        //Unload Content
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }//End UnloadContent method

        //Update
        protected override void Update(GameTime gameTime)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            if (Keyboard.GetState().IsKeyDown(Keys.Add))
                musicLevel = MathHelper.Clamp(musicLevel + 0.01f, 0.0f, 2.0f);
            if (Keyboard.GetState().IsKeyDown(Keys.Subtract))
                musicLevel = MathHelper.Clamp(musicLevel - 0.01f, 0.0f, 2.0f);
            

            // UPDATING PLAYING STATE
            switch (gameState)
            {
                #region Playing
                case State.Playing:
                    {
                        //for each enemy in our enemylists, update it and check for collisions
                        foreach (Enemy e in enemyList)
                        {
                            if (e.boundingBox.Intersects(player.boundingBox))
                            {
                                e.isVisible = false;
                                player.health -= Configuration.ENEMY_SHIP_COLLIDE_DAMAGE;
                            }

                            foreach (Bullet b in e.bulletList)
                            {
                                if (b.boundingBox.Intersects(player.boundingBox))
                                {
                                    b.isVisible = false;
                                    player.health -= Configuration.ENEMY_BULLET_COLLIDE_DAMAGE;
                                    AddPowerup();
                                }
                            }

                            //Check player bullet collision to enemy ship
                            foreach (Bullet b in player.bulletList)
                            {
                                if (b.boundingBox.Intersects(e.boundingBox))
                                {
                                    explosionList.Add(new Explosion(Content.Load<Texture2D>("explosion3"), new Vector2(e.position.X, e.position.Y)));
                                    AddPowerup();
                                    soundManager.explosionSound.Play();
                                    hud.playerScore += Configuration.ENEMY_SHIP_DESTROY_SCORE;
                                    e.isVisible = false;
                                    b.isVisible = false;
                                }
                            }
                            e.Update(gameTime);
                        }

                        foreach (Explosion ex in explosionList)
                        {
                            ex.Update(gameTime);
                        }


                        //for each asteroid in our asteroidlist, update it and check for collisions
                        foreach (Asteroid a in asteroidList)
                        {
                            if (a.boundingBox.Intersects(player.boundingBox))
                            {
                                a.isVisible = false;
                                player.health -= Configuration.ASTEROID_COLLIDE_DAMAGE;
                            }

                            //Iterate through our bulletlist, if any asteroids come in contacts with these bullets, destroy bullet and asteroid
                            foreach (Bullet b in player.bulletList)
                            {
                                if (a.boundingBox.Intersects(b.boundingBox))
                                {
                                    explosionList.Add(new Explosion(Content.Load<Texture2D>("explosion3"), new Vector2(a.position.X, a.position.Y)));
                                    soundManager.explosionSound.Play();
                                    hud.playerScore += Configuration.ASTEROID_DESTROY_SCORE;
                                    a.isVisible = false;
                                    b.isVisible = false;
                                }
                            }

                            a.Update(gameTime);
                        }

                        //Check powerup collision
                        foreach (Powerup p in powerupList)
                        {
                            //Player collides with powerup
                            if (p.boundingBox.Intersects(player.boundingBox))
                            {
                                p.isVisible = false;
                                if (p.powerupType == Powerup.PowerupType.Health)
                                {
                                    player.health += PowerupConstants.HEALTH_INCREASE;
                                    if (player.health > PlayerConstants.MAX_HEALTH)
                                    {
                                        player.health = PlayerConstants.MAX_HEALTH;
                                    }
                                }
                                else if (p.powerupType == Powerup.PowerupType.MachineGun)
                                {
                                    player.bulletDelayChange = PowerupConstants.RAPID_FIRE_DELAY_DECREASE;
                                    player.currentPlayerPowerup = Powerup.PowerupType.MachineGun;
                                    player.powerUpTime = PowerupConstants.RAPID_FIRE_TIME;
                                }
                            }
                            p.Update(gameTime);
                        }

                        //if playerhealth hits 0 then go to gameover state
                        if (player.health <= 0)
                        {
                            gameState = State.Gameover;
                        }
                        
                        hud.Update(gameTime);
                        player.Update(gameTime);
                        starField.Update(gameTime);
                        LoadAsteroids();
                        LoadEnemys();
                        ManageExplosion();
                        RemoveDeadPowerups();
                        break;
                    }

                #endregion

                #region Menu
                //UPDATING MENU STATE
                case State.Menu:
                    {

                        KeyboardState KBS = Keyboard.GetState();
                        if (KBS.IsKeyDown(Keys.Enter))
                        {
                            gameState = State.Playing;
                            soundManager.backgroundMusic.Play();
                            starField.speed = 5;
                            break;
                        }

                        starField.Update(gameTime);
                        starField.speed = 1;
                        break;
                    }
                #endregion

                #region Gameover
                //UPDATING GAMEOVER STATE
                case State.Gameover:
                    {
                        KeyboardState KBS = Keyboard.GetState();
                        if (KBS.IsKeyDown(Keys.Back))
                        {
                            gameState = State.Menu;
                            player.health = 200;
                            hud.playerScore = 0;
                            player.position.X = 300;
                            player.position.Y = 500;
                            enemyList.Clear();
                            asteroidList.Clear();
                            player.bulletList.Clear();
                        }

                        //stop music
                        //soundManager.backgroundMusic.
                        starField.Update(gameTime);
                        starField.speed = 1;
                        break;
                    }
                #endregion
            }

            base.Update(gameTime);
        }//End Update method

        //Draw
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            spriteBatch.Begin();

            switch (gameState)
            {
                #region Playing
                //Drawing playing state
                case State.Playing:
                    {
                        starField.Draw(spriteBatch);
                        hud.Draw(spriteBatch);
                        foreach (Asteroid a in asteroidList)
                        {
                            a.Draw(spriteBatch);
                        }

                        foreach (Enemy e in enemyList)
                        {
                            e.Draw(spriteBatch);
                        }

                        foreach (Explosion ex in explosionList)
                        {
                            ex.Draw(spriteBatch);
                        }

                        foreach (Powerup p in powerupList)
                        {
                            p.Draw(spriteBatch);
                        }
                        player.Draw(spriteBatch);
                        break;
                    }
                #endregion

                #region Menu
                //Drawing menu state
                case State.Menu:
                    {
                        starField.Draw(spriteBatch);
                        spriteBatch.Draw(menuImage, new Vector2(0, 0), Color.White);
                        break;
                    }
                #endregion

                #region Gameover
                //Drawing gameover state
                case State.Gameover:
                    {
                        starField.Draw(spriteBatch);
                        spriteBatch.Draw(gameoverImage, new Vector2(0, 0), Color.White);
                        spriteBatch.DrawString(hud.playerScoreFont, "Final Score: " + hud.playerScore.ToString(), new Vector2(200, 100), Color.White);
                        break;
                    }
                #endregion
            }
            
            spriteBatch.End();
            base.Draw(gameTime);
        }//End Draw method

        //Load asteroids
        public void LoadAsteroids()
        {
            int randY = random.Next(-600, -50);
            int randX = random.Next(0, 550);

            if (asteroidList.Count < 5)
            {
                asteroidList.Add(new Asteroid(Content.Load<Texture2D>("asteroid"), new Vector2(randX, randY)));
            }

            for (int i = 0; i < asteroidList.Count; i++)
            {
                if (!asteroidList[i].isVisible)
                {
                    asteroidList.RemoveAt(i);
                    i--;
                }
            }
        }//End LoadAsteroids function

        //Load enemy
        public void LoadEnemys()
        {
            int randY = random.Next(-600, -100);
            int randX = random.Next(0, 550);

            if (enemyList.Count < 3)
            {
                enemyList.Add(new Enemy(Content.Load<Texture2D>("enemyship"), new Vector2(randX, randY), Content.Load<Texture2D>("EnemyBullet")));
            }

            for (int i = 0; i < enemyList.Count; i++)
            {
                if (!enemyList[i].isVisible)
                {
                    enemyList.RemoveAt(i);
                    i--;
                }
            }
        }//End LoadEnemys function

        //Manager Explosion
        public void ManageExplosion()
        {
            for (int i = 0; i < explosionList.Count; i++)
            {
                if (!explosionList[i].isVisible)
                {
                    explosionList.RemoveAt(i);
                    i--;
                }
            }
        }//End ManageExplosion method

        public void AddPowerup()
        {
            int randChance = random.Next(0, 10);
            if (randChance > 6)
            {
                int randY = random.Next(-600, -50);
                int randX = random.Next(0, 550);
                int randPowerup = random.Next(0, 2);

                if (powerupList.Count < 5)
                {
                    if (randPowerup == 0)
                        powerupList.Add(new Powerup(Content.Load<Texture2D>("health"), new Vector2(randX, randY)));
                    else if (randPowerup == 1)
                        powerupList.Add(new Powerup(Content.Load<Texture2D>("rapidfire"), new Vector2(randX, randY), Powerup.PowerupType.MachineGun));
                }
            }
        }//End AddPowerup method

        public void RemoveDeadPowerups()
        {

            for(int i = 0; i < powerupList.Count; i++)
            {
                if (!powerupList[i].isVisible)
                {
                    powerupList.RemoveAt(i);
                    i--;
                }
            }
        }//End RemoveDeadPowerups method

    }
}
