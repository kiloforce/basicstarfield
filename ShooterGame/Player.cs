﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace ShooterGame
{
    public class Player
    {
        
        public Texture2D texture, bulletTexture;
        public Vector2 position;
        public int speed, health;
        public float bulletDelay, bulletDelayChange;
        public Rectangle boundingBox;
        public bool isCollliding;
        public List<Bullet> bulletList;
        SoundManager soundManager = new SoundManager();
        public static Player player;
        public int powerUpTime = 0;
        public Powerup.PowerupType? currentPlayerPowerup;

        //Constructor
        public Player()
        {
            bulletList = new List<Bullet>();
            texture = null;
            position = new Vector2(300, 500);
            bulletDelay = 1;
            bulletDelayChange = 0;
            speed = PlayerConstants.NORMAL_SPEED;
            isCollliding = false;
            health = PlayerConstants.MAX_HEALTH;
            player = this;
            currentPlayerPowerup = null;
        }

        //Load Content
        public void LoadContent(ContentManager Content)
        {
            texture = Content.Load<Texture2D>("ship");
            bulletTexture = Content.Load<Texture2D>("playerbullet");
            soundManager.LoadContent(Content);
        }

        //Draw
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, position, Color.White);
            foreach (Bullet b in bulletList)
            {
                b.Draw(spriteBatch);
            }
        }

        //Update
        public void Update(GameTime gameTime)
        {
            //Getting keyboard state
            KeyboardState KBS = Keyboard.GetState();

            //BoundingBox for our playership
            boundingBox = new Rectangle((int)position.X, (int)position.Y, texture.Width, texture.Height); 

            //fire bullets

            if (KBS.IsKeyUp(Keys.Space))
            {
                bulletDelay = 1;
            }
            if (KBS.IsKeyDown(Keys.Space))
            {
                Shoot();
            }

            UpdateBullets();

            //Ship Controls
            if (KBS.IsKeyDown(Keys.W))
            {
                position.Y = position.Y - speed;
            }

            if (KBS.IsKeyUp(Keys.W))
            {
                position.Y = position.Y + 5;
            }

            if (KBS.IsKeyDown(Keys.A))
            {
                position.X = position.X - speed;
            }

            if (KBS.IsKeyDown(Keys.S))
            {
                position.Y = position.Y + speed;
            }

            if (KBS.IsKeyDown(Keys.D))
            {
                position.X = position.X + speed;
            }

            //Keep player ship in screen bounds
            if (position.X <= 0)
                position.X = 0;

            if (position.X >= 600 - texture.Width)
                position.X = 600 - texture.Width;

            if (position.Y <= 0)
                position.Y = 0;

            if (position.Y >= 600 - texture.Height)
                position.Y = 600 - texture.Height;

        }

        //Shoot Method: used to set starting position of our bullets
        public void Shoot()
        {
            //Shoot only if bullet delay resets
            if (bulletDelay > 0)
            {
                bulletDelay--;
            }

            //If bulletdelay is at 0, create new bullet at player position and make it visible on the screen and add to list
            if (bulletDelay <= 0)
            {
                soundManager.playerShootSound.Play();
                Bullet newBullet = new Bullet(bulletTexture);
                newBullet.position = new Vector2(position.X + 32 - newBullet.texture.Width / 2, position.Y + 30);
                newBullet.isVisible = true;

                if (bulletList.Count < 20)
                {
                    bulletList.Add(newBullet);
                    ManagePowerups();
                }
                bulletDelay = PlayerConstants.NORMAL_BULLET_DELAY - bulletDelayChange;
            }
        }//End Shoot function

        //Update Bullets
        public void UpdateBullets()
        {
            //for each bullet in our bulletlist, update the movement and if the bullet hits the top of the screen remove it from list
            foreach (Bullet b in bulletList)
            {
                //Boundingbox for bullets
                b.boundingBox = new Rectangle((int)b.position.X, (int)b.position.Y, 5, 5);

                //set movement for bullet
                b.position.Y = b.position.Y - b.speed;

                //if bullet hits the top of the screen, then make visible false
                if (b.position.Y <= 0)
                {
                    b.isVisible = false;
                }
            }

            //Iterate through bulletlist and see if any of the bullets are not visible, if they arent then remove from bulletlist
            for (int i = 0; i < bulletList.Count; i++)
            {
                if (!bulletList[i].isVisible)
                {
                    bulletList.RemoveAt(i);
                    i--;
                }
            }
        }//End UpdateBullets method

        public void ManagePowerups()
        {
            powerUpTime -= 1;

            if (powerUpTime <= 0 && currentPlayerPowerup != null)
            {
                if (currentPlayerPowerup == Powerup.PowerupType.MachineGun)
                {
                    player.bulletDelayChange = 0;
                    currentPlayerPowerup = null;
                }
            }
        }//End ManagePowerups method
    }
}
