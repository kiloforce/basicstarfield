﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;

namespace ShooterGame
{
    public class SoundManager
    {
        public SoundEffect playerShootSound, explosionSound, backgroundMusic;

        //Constructor
        public SoundManager()
        {
            playerShootSound = null;
            explosionSound = null;
            backgroundMusic = null;
        }

        //Load Content
        public void LoadContent(ContentManager Content)
        {
            playerShootSound = Content.Load<SoundEffect>("playershoot");
            explosionSound = Content.Load<SoundEffect>("explode");
            backgroundMusic = Content.Load<SoundEffect>("epic1");
        }
        
    }
}
